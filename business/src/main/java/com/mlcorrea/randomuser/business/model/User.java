package com.mlcorrea.randomuser.business.model;

import com.mlcorrea.randomuser.business.model.error.ErrorsModel;
import com.mlcorrea.randomuser.business.utils.Validation;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * Created by manuel on 10/06/17.
 */

public class User implements Serializable, Validation {

    private static final String USER_FULL_NAME = "%s %s";

    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private String gender;
    private String phone;
    private Photo photo;
    private String dateRegistered;
    private UserLocation location;


    public String getFullName() {
        return String.format(USER_FULL_NAME, this.firstName, this.lastName);
    }

    @Override
    public void validate() {
        if (StringUtils.isEmpty(this.id)) {
            throw new ErrorsModel(ErrorsModel.VALIDATION_ERROR_INVALID_USER_ID);
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public String getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(String dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public UserLocation getLocation() {
        return location;
    }

    public void setLocation(UserLocation location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", phone='" + phone + '\'' +
                ", photo=" + photo +
                ", dateRegistered='" + dateRegistered + '\'' +
                ", location=" + location +
                '}';
    }
}
