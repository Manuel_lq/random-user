package com.mlcorrea.randomuser.business.model.error;


/**
 * Created by manuel on 10/06/17.
 */

public class ErrorsModel extends RuntimeException {

    //error when we could not parse a successful response
    public static final int ERROR_PARSING_JSON = 11;
    //error when we could not wrap to a business entity
    public static final int ERROR_WRAPPING_TO_BUSINESS_ENTITY = 12;
    //Error when something unexpected happen
    public static final int ERROR_EMPTY_RESPONSE = 13;
    public static final int ERROR_GENERIC = 14;
    public static final int ERROR_WRAPPING_TO_DB_ENTITY = 15;
    public static final int VALIDATION_ERROR_INVALID_USER_ID = 16;

    //error when we could not parse a successful response
    public static final int ERROR_NO_NETWORK = 503;

    public static final int ERROR_USER_UNAUTHORIZED = 401;


    private String resource;
    private String requestId;
    private String errorCode;
    private String errorMessage;
    private int redCodeHTTP;


    public ErrorsModel(int httpError) {
        this.redCodeHTTP = httpError;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public int getRedCodeHTTP() {
        return redCodeHTTP;
    }

    public void setRedCodeHTTP(int redCodeHTTP) {
        this.redCodeHTTP = redCodeHTTP;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean shouldRetry() {
        return redCodeHTTP < 400 || redCodeHTTP > 499 && redCodeHTTP != ErrorsModel.ERROR_USER_UNAUTHORIZED;
    }
}
