package com.mlcorrea.randomuser.business.contract;

import android.support.annotation.NonNull;

/**
 * Created by manuel on 10/06/17.
 */

public interface ErrorMessageStrings {

    /**
     * Get generic error messages from the app
     *
     * @return {@link String }
     */
    @NonNull
    String getGenericErrorMessage();

    /**
     * Get No internet error messages from the app
     *
     * @return {@link String }
     */
    @NonNull
    String getMessageNotInternetConnection();

    /**
     * Get parsing error messages from the app
     *
     * @return {@link String }
     */
    @NonNull
    String getMessageErrorParsingResponse();

    /**
     * Get empty error messages from the app
     *
     * @return {@link String }
     */
    @NonNull
    String getMessageErrorEmptyResponse();
}
