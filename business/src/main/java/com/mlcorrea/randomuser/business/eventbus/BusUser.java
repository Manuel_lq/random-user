package com.mlcorrea.randomuser.business.eventbus;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by manuel on 10/06/17.
 */

public class BusUser {
    public enum TypeBusUser {
        GET_USER_LIST_SUCCESS,
        ON_ERROR,
    }

    public final TypeBusUser typeBusUser;

    public final Object object;

    public final String tag;

    public BusUser(@NonNull TypeBusUser typeBusUser, @Nullable Object object, @NonNull String tag) {
        this.typeBusUser = typeBusUser;
        this.object = object;
        this.tag = tag;
    }
}
