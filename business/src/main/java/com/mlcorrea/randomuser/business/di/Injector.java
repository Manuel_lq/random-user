package com.mlcorrea.randomuser.business.di;

/**
 * Created by manuel on 10/06/17.
 */

public interface Injector {
    void injectClass(InjectClassToComponent injectClassToComponent);
}
