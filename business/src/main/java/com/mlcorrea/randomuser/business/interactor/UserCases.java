package com.mlcorrea.randomuser.business.interactor;

import android.support.annotation.NonNull;

/**
 * Created by manuel on 10/06/17.
 */

public interface UserCases {

    /**
     * Get the paginated User list
     *
     * @param page    {@link Integer} number of the page to fetch
     * @param results {@link Integer} number of user in the page
     * @param abc     {@link String} code for pagination
     * @param tag     {@link String} Request TAG
     */
    void getUserList(int page, int results, @NonNull String abc,@NonNull String tag);
}
