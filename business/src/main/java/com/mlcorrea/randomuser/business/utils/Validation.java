package com.mlcorrea.randomuser.business.utils;

/**
 * Created by manuel on 11/06/17.
 */

public interface Validation {

    void validate();
}
