package com.mlcorrea.randomuser.business.repository;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;

import java.util.List;

/**
 * Created by manuel on 10/06/17.
 */

public interface UserRepository {

    /**
     * Call back from the DB user Table
     */
    interface UserCallback {
        void userList(@Nullable List<User> userList);

        void onError(@NonNull Exception e);
    }

    /**
     * Add User listener to the DB
     *
     * @param userCallback {@link UserCallback}
     * @param tag          {@link String}
     */
    void addListerUserTable(@NonNull UserCallback userCallback, @NonNull String tag);

    /**
     * Get the list of user filtered sync
     *
     * @param filter {@link String }
     * @return {@link List<User>}
     */
    @Nullable
    List<User> getFilterUserList(@NonNull String filter);

    /**
     * Get the user with reference
     *
     * @param userId {@link String}
     * @return {@link User}
     */
    @Nullable
    User getUserById(String userId);

    /**
     * Remove user Listener from Db
     *
     * @param userCallback {@link UserCallback}
     */
    void removeListenerUserTable(@NonNull UserCallback userCallback);

    /**
     * Mark user as delete
     *
     * @param userId {@link String }
     */
    void deleteUserFromDB(@Nullable String userId);
}
