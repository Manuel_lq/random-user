package com.mlcorrea.randomuser.data.database;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.data.database.model.UserDB;

import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by manuel on 10/06/17.
 */

public interface UserModel {

    /**
     * Fetch the user LIst from the DB
     *
     * @return {@link RealmResults<UserDB>}
     */
    RealmResults<UserDB> getUserListAsync();

    /**
     * Get the user from DB by userId
     *
     * @param userId {@link String }
     * @return {@link UserDB}
     */
    @Nullable
    UserDB getUserById(@Nullable String userId);

    /**
     * Fetch the user list filtered  from db
     *
     * @param filter {@link String}
     * @return {@link RealmResults<UserDB>}
     */
    RealmResults<UserDB> filterUserList(@NonNull String filter);

    /**
     * Add or update the list of user
     *
     * @param userDBRealmList {@link RealmList<UserDB>}
     */
    void addOrUpdateUserList(@Nullable RealmList<UserDB> userDBRealmList);

    /**
     * Mark a user as delete in DB
     *
     * @param id {@link String }
     */
    void markUserAsDelete(@Nullable String id);
}
