package com.mlcorrea.randomuser.data.database.implentation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.business.repository.UserRepository;
import com.mlcorrea.randomuser.data.database.UserModel;
import com.mlcorrea.randomuser.data.database.model.UserDB;
import com.mlcorrea.randomuser.data.database.wrapper.UserBDWrapper;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import javax.inject.Inject;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by manuel on 10/06/17.
 */

public class UserRepositoryImpl implements UserRepository {

    private final UserModel userModel;
    private final UserBDWrapper userBDWrapper;

    private Map<UserCallback, String> userTableListener = Collections.synchronizedMap(new WeakHashMap<UserCallback, String>());

    private RealmChangeListener<RealmResults<UserDB>> addUserBDListener;
    private RealmResults<UserDB> userDBListResult;

    @Inject
    public UserRepositoryImpl(UserModel userModel, UserBDWrapper userBDWrapper) {
        this.userModel = userModel;
        this.userBDWrapper = userBDWrapper;

        addDatabaseListener();
    }


    @Override
    public synchronized void addListerUserTable(@NonNull UserCallback userCallback, @NonNull String tag) {
        this.userTableListener.put(userCallback, tag);
        addRealmListener();
    }

    @Nullable
    @Override
    public List<User> getFilterUserList(@NonNull String filter) {
        try {
            return this.userBDWrapper.userListWrapper(userModel.filterUserList(filter));
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    @Override
    public User getUserById(@Nullable String userId) {

        UserDB userDB = userModel.getUserById(userId);
        if (userDB == null) {
            return null;
        }
        try {
            return userBDWrapper.userWrapper(userDB);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public synchronized void removeListenerUserTable(@NonNull UserCallback userCallback) {
        this.userTableListener.remove(userCallback);
        if (this.userTableListener.isEmpty()) {
            removeRealmListener();
        }
    }

    @Override
    public void deleteUserFromDB(String userId) {
        userModel.markUserAsDelete(userId);
    }

    /*----------------------PRIVATE METHOD-------------------*/

    private void addRealmListener() {
        if (this.userDBListResult != null) {
            return;
        }
        this.userDBListResult = this.userModel.getUserListAsync();
        this.userDBListResult.addChangeListener(this.addUserBDListener);
    }

    private void removeRealmListener() {
        if (this.userDBListResult == null) {
            return;
        }
        this.userDBListResult.removeChangeListener(this.addUserBDListener);
        this.userDBListResult = null;

    }

    private void onChangeUserTable(final RealmResults<UserDB> userRealmResults) {
        if (userRealmResults == null) {
            return;
        }
        if (!userRealmResults.isValid() || !userRealmResults.isLoaded()) {
            return;
        }
        synchronized (userTableListener) {
            for (Map.Entry<UserCallback, String> entry : userTableListener.entrySet()) {
                if (entry.getKey() != null) {
                    try {
                        entry.getKey().userList(this.userBDWrapper.userListWrapper(userRealmResults));
                    } catch (Exception e) {
                        entry.getKey().onError(e);
                    }
                }
            }
        }
    }

    /*----------------CALL BACKS----------------------*/

    private void addDatabaseListener() {
        this.addUserBDListener = new RealmChangeListener<RealmResults<UserDB>>() {
            @Override
            public void onChange(RealmResults<UserDB> userRealmResults) {
                onChangeUserTable(userRealmResults);
            }
        };
    }


}
