package com.mlcorrea.randomuser.data.job.request;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;
import com.mlcorrea.randomuser.data.database.UserModel;
import com.mlcorrea.randomuser.business.di.InjectClassToComponent;
import com.mlcorrea.randomuser.business.eventbus.BusUser;
import com.mlcorrea.randomuser.business.model.error.ErrorsModel;
import com.mlcorrea.randomuser.data.database.model.UserDB;
import com.mlcorrea.randomuser.data.job.base.BaseJob;
import com.mlcorrea.randomuser.data.network.api.ApiService;
import com.mlcorrea.randomuser.data.network.dto.UserResponseDto;
import com.mlcorrea.randomuser.data.network.wrapper.UserDtoWrapper;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.realm.RealmList;
import retrofit2.Call;
import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 *
 * Fetch User list
 */

public class GetListUserJob extends BaseJob<UserResponseDto> implements InjectClassToComponent {

    @SuppressWarnings("WeakerAccess")
    @Inject
    transient ApiService apiService;
    @SuppressWarnings("WeakerAccess")
    @Inject
    transient EventBus eventBus;
    @SuppressWarnings("WeakerAccess")
    @Inject
    transient UserDtoWrapper userDtoWrapper;
    @SuppressWarnings("WeakerAccess")
    @Inject
    transient UserModel userModel;


    private final String jobQueueTag;
    private final int page;
    private final int results;
    private final String abc;

    public GetListUserJob(@Priority int priority, int page, int results, @NonNull String abc, @NonNull String tag) {
        super(new Params(priority).requireNetwork().addTags(tag));

        this.jobQueueTag = tag;
        this.page = page;
        this.results = results;
        this.abc = abc;
    }


    @Override
    protected InjectClassToComponent getTheClass() {
        return this;
    }

    @Override
    public void onAdded() {

    }

    @NonNull
    @Override
    public Call<UserResponseDto> executeRequest() throws Throwable {
        return this.apiService.getUserList(String.valueOf(this.page), String.valueOf(this.results), this.abc);
    }

    @Override
    public void onSuccess(@Nullable UserResponseDto userResponseDto) throws Throwable {
        RealmList<UserDB> userDBRealmList;
        try {
            userDBRealmList = this.userDtoWrapper.responseUserDtoWrapper(userResponseDto);
        } catch (Exception e) {
            throw new ErrorsModel(ErrorsModel.ERROR_WRAPPING_TO_DB_ENTITY);
        }
        //save user list in DB
        this.userModel.addOrUpdateUserList(userDBRealmList);
        this.eventBus.post(new BusUser(BusUser.TypeBusUser.GET_USER_LIST_SUCCESS, null, this.jobQueueTag));
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {
        Timber.d(throwable, "onCanceledFinal");
        this.eventBus.post(new BusUser(BusUser.TypeBusUser.ON_ERROR, throwable, this.jobQueueTag));
    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        Timber.d("shouldReRunOnThrowable");
        if (shouldRetry(throwable)) {
            return RetryConstraint.createExponentialBackoff(runCount, 1000);
        }
        return RetryConstraint.CANCEL;
    }
}
