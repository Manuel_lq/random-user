package com.mlcorrea.randomuser.data.database.wrapper.implementation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.Photo;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.business.model.UserLocation;
import com.mlcorrea.randomuser.data.database.model.BasicUserDetailsDB;
import com.mlcorrea.randomuser.data.database.model.LocationDB;
import com.mlcorrea.randomuser.data.database.model.PhotoDB;
import com.mlcorrea.randomuser.data.database.model.UserDB;
import com.mlcorrea.randomuser.data.database.wrapper.UserBDWrapper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmResults;

/**
 * Created by manuel on 10/06/17.
 */

public class UserDBWrapperImpl implements UserBDWrapper {

    @Inject
    public UserDBWrapperImpl() {
    }

    @Nullable
    @Override
    public List<User> userListWrapper(@Nullable RealmResults<UserDB> userDBRealmList) throws Exception {
        if (userDBRealmList == null) {
            return null;
        }

        List<User> userList = new ArrayList<>();
        for (UserDB userDB : userDBRealmList) {
            userList.add(userWrapper(userDB));
        }

        return userList;
    }

    @NonNull
    public User userWrapper(@NonNull UserDB userDB) throws Exception {

        User user = new User();

        user.setId(userDB.getId());
        user.setEmail(userDB.getEmail());
        user.setFirstName(userDB.getFirstName());
        user.setLastName(userDB.getLastName());

        BasicUserDetailsDB basicUserDetailsDB = userDB.getBasicUserDetailsDB();
        if (basicUserDetailsDB == null) {
            return user;
        }

        user.setDateRegistered(basicUserDetailsDB.getDateRegistered());
        user.setGender(basicUserDetailsDB.getGender());
        user.setPhone(basicUserDetailsDB.getPhone());

        LocationDB locationDB = basicUserDetailsDB.getLocation();
        if (locationDB != null) {
            UserLocation userLocation = new UserLocation();
            userLocation.setStreet(locationDB.getStreet());
            userLocation.setCity(locationDB.getCity());
            userLocation.setState(locationDB.getState());
            user.setLocation(userLocation);
        }
        PhotoDB photoDB = basicUserDetailsDB.getPhoto();
        if (photoDB != null) {
            Photo photo = new Photo();
            photo.setLarge(photoDB.getLarge());
            photo.setMedium(photoDB.getMedium());
            photo.setThumbnail(photoDB.getThumbnail());
            user.setPhoto(photo);
        }

        return user;
    }

}
