package com.mlcorrea.randomuser.data.network.request;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;
import com.mlcorrea.randomuser.business.interactor.UserCases;
import com.mlcorrea.randomuser.data.job.base.BaseJob;
import com.mlcorrea.randomuser.data.job.request.GetListUserJob;

import javax.inject.Inject;

/**
 * Created by manuel on 10/06/17.
 */

public class UserCasesImpl implements UserCases {

    private final JobManager jobManager;

    @Inject
    public UserCasesImpl(JobManager jobManager) {
        this.jobManager = jobManager;
    }

    @Override
    public void getUserList(int page, int results, @NonNull String abc, @NonNull String tag) {
        this.jobManager.addJobInBackground(new GetListUserJob(BaseJob.BACKGROUND, page, results, abc, tag));
    }
}
