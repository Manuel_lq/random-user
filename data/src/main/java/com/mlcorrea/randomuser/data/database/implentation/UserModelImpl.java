package com.mlcorrea.randomuser.data.database.implentation;

import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.data.database.UserModel;
import com.mlcorrea.randomuser.data.database.model.UserDB;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by manuel on 10/06/17.
 */

public class UserModelImpl implements UserModel {

    private final Realm realmMain;

    @Inject
    public UserModelImpl(Realm realmMain) {
        this.realmMain = realmMain;
    }

    @Override
    public RealmResults<UserDB> getUserListAsync() {
        return this.realmMain.where(UserDB.class)
                .equalTo("deleted", false)
                .findAllSortedAsync("firstName", Sort.ASCENDING);
    }

    @Nullable
    @Override
    public UserDB getUserById(@Nullable String userId) {
        if (StringUtils.isEmpty(userId)){
            return null;
        }
        return this.realmMain.where(UserDB.class)
                .equalTo("id", userId)
                .findFirst();
    }

    @Override
    public RealmResults<UserDB> filterUserList(@NonNull String filter) {
        return this.realmMain.where(UserDB.class)
                .equalTo("deleted", false)
                .contains("firstName", filter)
                .or()
                .contains("lastName", filter)
                .or()
                .contains("email", filter)
                .findAllSorted("firstName", Sort.ASCENDING);

    }

    @Override
    public void addOrUpdateUserList(@Nullable RealmList<UserDB> userDBRealmList) {
        if (userDBRealmList == null) {
            return;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            addOrUpdateBDUserList(realmMain, userDBRealmList);
        } else {
            final Realm realm = Realm.getDefaultInstance();
            addOrUpdateBDUserList(realm, userDBRealmList);
            realm.close();
        }
    }

    @Override
    public void markUserAsDelete(@Nullable String id) {
        if (StringUtils.isEmpty(id)) {
            return;
        }
        if (Looper.myLooper() == Looper.getMainLooper()) {
            markAsDeleteBDUser(realmMain, id);
        } else {
            final Realm realm = Realm.getDefaultInstance();
            markAsDeleteBDUser(realm, id);
            realm.close();
        }
    }

    /*--------------------------PRIVATE METHOD---------------*/

    private void addOrUpdateBDUserList(@NonNull Realm realm, @NonNull RealmList<UserDB> userDBRealmList) {
        realm.beginTransaction();
        for (UserDB userDB : userDBRealmList) {

            UserDB userFound = realm.where(UserDB.class)
                    .equalTo("id", userDB.getId())
                    .findFirst();

            if (userFound != null) {
                userFound.setLastName(userDB.getLastName());
                userFound.setFirstName(userDB.getFirstName());
                userFound.setEmail(userDB.getEmail());
                userFound.setBasicUserDetailsDB(userDB.getBasicUserDetailsDB());
            } else {
                realm.copyToRealmOrUpdate(userDB);
            }
        }
        realm.commitTransaction();
    }

    private void markAsDeleteBDUser(@NonNull Realm realm, @NonNull final String id) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                UserDB userFound = realm.where(UserDB.class)
                        .equalTo("id", id)
                        .findFirst();
                userFound.setDeleted(true);
            }
        });
    }
}
