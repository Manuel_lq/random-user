package com.mlcorrea.randomuser.data.database.wrapper;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.data.database.model.UserDB;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by manuel on 10/06/17.
 */

public interface UserBDWrapper {

    /**
     * Wrap the data coming from DB to a entity object
     *
     * @param userDBRealmList {@link RealmResults<UserDB>}
     * @return {@link List<User>}
     * @throws Exception
     */
    @Nullable
    List<User> userListWrapper(@Nullable RealmResults<UserDB> userDBRealmList) throws Exception;

    /**
     * Wrap a single instance of user
     *
     * @param userDB {@link UserDB}
     * @return {@link User}
     * @throws Exception
     */
    @NonNull
    User userWrapper(@NonNull UserDB userDB) throws Exception;
}
