package com.mlcorrea.randomuser.data.job.base;

import android.app.Application;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.mlcorrea.randomuser.business.di.InjectClassToComponent;
import com.mlcorrea.randomuser.business.di.Injector;
import com.mlcorrea.randomuser.business.model.error.ErrorsModel;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.HttpURLConnection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import timber.log.Timber;

import static com.mlcorrea.randomuser.business.model.error.ErrorsModel.ERROR_EMPTY_RESPONSE;
import static com.mlcorrea.randomuser.business.model.error.ErrorsModel.ERROR_NO_NETWORK;
import static com.mlcorrea.randomuser.business.model.error.ErrorsModel.ERROR_PARSING_JSON;
import static com.mlcorrea.randomuser.business.model.error.ErrorsModel.ERROR_USER_UNAUTHORIZED;
import static com.mlcorrea.randomuser.business.model.error.ErrorsModel.ERROR_WRAPPING_TO_BUSINESS_ENTITY;
import static com.mlcorrea.randomuser.business.model.error.ErrorsModel.ERROR_WRAPPING_TO_DB_ENTITY;

/**
 * Created by manuel on 10/06/17.
 */

public abstract class BaseJob<Result> extends Job {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ERROR_PARSING_JSON, ERROR_NO_NETWORK, ERROR_EMPTY_RESPONSE, ERROR_USER_UNAUTHORIZED, ERROR_WRAPPING_TO_BUSINESS_ENTITY, ERROR_WRAPPING_TO_DB_ENTITY})
    public @interface Error {
    }

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({UI_HIGH, BACKGROUND})
    public @interface Priority {

    }

    public static final int UI_HIGH = 10;
    public static final int BACKGROUND = 1;


    public BaseJob(Params params) {
        super(params);
    }

    @NonNull
    protected abstract Call<Result> executeRequest() throws Throwable;

    protected abstract void onSuccess(@Nullable Result result) throws Throwable;

    protected abstract InjectClassToComponent getTheClass();


    @Override
    public void onRun() throws Throwable {

        onSuccess(startApiRequest(executeRequest()));
    }

    /**
     * Inject the Job to the application component
     *
     * @param application {@link Application}
     */
    public void inject(Application application) {
        ((Injector) application).injectClass(getTheClass());
    }

    /*------------------------PRIVATE METHOD--------------------*/

    private Result startApiRequest(@NonNull Call<Result> retrofitCall) throws Throwable {
        Response<Result> response;
        try {
            response = retrofitCall.execute();
        } catch (Throwable throwable) {
            throw checkErrorResponse(throwable);
        }

        if (response == null) {
            //send generic error
            Timber.tag(getClassName()).i("REQUEST ERROR: Null response");
            throw new ErrorsModel(ErrorsModel.ERROR_EMPTY_RESPONSE);
        }

        if (response.isSuccessful()) {
            Timber.tag(getClassName()).d("REQUEST SUCCESSFUL");
            return response.body();
        }

        handleErrorResponse(response);

        return null;
    }

    private void handleErrorResponse(@NonNull Response<Result> response) throws Throwable {
        //error found
        int statusCode = response.code();
        Timber.tag(getClassName()).i("REQUEST ERROR: Http error code: %d", statusCode);

        if (statusCode >= HttpURLConnection.HTTP_INTERNAL_ERROR) {
            Timber.tag(getClassName()).e("REQUEST ERROR: Code: %d", statusCode);
        }

        // handle request errors
        ResponseBody errorBody = response.errorBody();
        if (errorBody != null) {
            //TODO parse the error message
            //Generic error
            throw new ErrorsModel(statusCode);
        } else {
            Timber.tag(getClassName()).w("REQUEST ERROR: response with null body");
            //Generic error
            throw new ErrorsModel(statusCode);
        }
    }

    private Throwable checkErrorResponse(Throwable throwable) {

        if (throwable instanceof IOException) {
            // A network or conversion error happened
            Timber.tag(getClassName()).i("REQUEST ERROR: A network error %s", throwable.getMessage());
            return new ErrorsModel(ErrorsModel.ERROR_NO_NETWORK);
        }
        Timber.tag(getClassName()).i("REQUEST ERROR: A unknown error %s", throwable.getMessage());
        return new ErrorsModel(ErrorsModel.ERROR_EMPTY_RESPONSE);
    }

    private String getClassName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Check if {@link Throwable} should retry
     *
     * @param throwable {@link Throwable}
     * @return {@link Boolean}
     */
    protected boolean shouldRetry(Throwable throwable) {
        if (throwable instanceof ErrorsModel) {
            ErrorsModel exception = (ErrorsModel) throwable;
            return exception.shouldRetry();
        }
        return true;
    }

    @Override
    protected int getRetryLimit() {
        return 2;
    }
}
