package com.mlcorrea.randomuser.data.database.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by manuel on 10/06/17.
 */

public class BasicUserDetailsDB extends RealmObject {

    @PrimaryKey
    private String id;

    private String gender;
    private String phone;
    private PhotoDB photo;
    private String dateRegistered;
    private LocationDB location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public PhotoDB getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoDB photo) {
        this.photo = photo;
    }

    public String getDateRegistered() {
        return dateRegistered;
    }

    public void setDateRegistered(String dateRegistered) {
        this.dateRegistered = dateRegistered;
    }

    public LocationDB getLocation() {
        return location;
    }

    public void setLocation(LocationDB location) {
        this.location = location;
    }
}
