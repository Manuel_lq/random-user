package com.mlcorrea.randomuser.data.database.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by manuel on 10/06/17.
 */

public class UserDB extends RealmObject {

    @PrimaryKey
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private boolean deleted;
    private BasicUserDetailsDB basicUserDetailsDB;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public BasicUserDetailsDB getBasicUserDetailsDB() {
        return basicUserDetailsDB;
    }

    public void setBasicUserDetailsDB(BasicUserDetailsDB basicUserDetailsDB) {
        this.basicUserDetailsDB = basicUserDetailsDB;
    }
}
