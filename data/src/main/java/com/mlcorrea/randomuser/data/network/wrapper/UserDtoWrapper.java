package com.mlcorrea.randomuser.data.network.wrapper;

import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.data.database.model.UserDB;
import com.mlcorrea.randomuser.data.network.dto.UserResponseDto;

import io.realm.RealmList;

/**
 * Created by manuel on 10/06/17.
 */

public interface UserDtoWrapper {

    @Nullable
    RealmList<UserDB> responseUserDtoWrapper(@Nullable UserResponseDto userResponseDto) throws Exception;
}
