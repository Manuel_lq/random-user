package com.mlcorrea.randomuser.data.network.api;

import com.mlcorrea.randomuser.data.network.dto.UserResponseDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by manuel on 10/06/17.
 */

public interface ApiService {

    @GET("/api")
    Call<UserResponseDto> getUserList(@Query("page") String page, @Query("results") String results, @Query("abc") String abc);


}
