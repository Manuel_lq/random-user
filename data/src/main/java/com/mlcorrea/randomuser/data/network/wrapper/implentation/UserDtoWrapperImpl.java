package com.mlcorrea.randomuser.data.network.wrapper.implentation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.data.database.model.BasicUserDetailsDB;
import com.mlcorrea.randomuser.data.database.model.LocationDB;
import com.mlcorrea.randomuser.data.database.model.PhotoDB;
import com.mlcorrea.randomuser.data.database.model.UserDB;
import com.mlcorrea.randomuser.data.network.dto.UserDto;
import com.mlcorrea.randomuser.data.network.dto.UserResponseDto;
import com.mlcorrea.randomuser.data.network.wrapper.UserDtoWrapper;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

import io.realm.RealmList;

/**
 * Created by manuel on 10/06/17.
 */

public class UserDtoWrapperImpl implements UserDtoWrapper {

    @Inject
    public UserDtoWrapperImpl() {
    }

    @Nullable
    @Override
    public RealmList<UserDB> responseUserDtoWrapper(@Nullable UserResponseDto userResponseDto) throws Exception {
        if (userResponseDto == null) {
            throw new Exception("Invalid Response");
        }
        if (userResponseDto.getUserList() == null) {
            return null;
        }

        RealmList<UserDB> userDBRealmList = new RealmList<>();
        for (UserDto userDto : userResponseDto.getUserList()) {
            userDBRealmList.add(userDtoWrapper(userDto));
        }

        return userDBRealmList;
    }

    @NonNull
    private static UserDB userDtoWrapper(@NonNull UserDto userDto) throws Exception {

        if (userDto.getLoginDto() == null || StringUtils.isEmpty(userDto.getLoginDto().getMd5())) {
            throw new Exception("Invalid User ID");
        }
        UserDB userDB = new UserDB();

        userDB.setId(userDto.getLoginDto().getMd5());
        userDB.setBasicUserDetailsDB(userDetailsWrapper(userDto));
        userDB.setEmail(userDto.getEmail());
        if (userDto.getNameDto() != null) {
            userDB.setFirstName(userDto.getNameDto().getFirst());
            userDB.setLastName(userDto.getNameDto().getLast());
        }

        return userDB;
    }

    @NonNull
    private static BasicUserDetailsDB userDetailsWrapper(@NonNull UserDto userDto) {
        BasicUserDetailsDB basicUserDetailsDB = new BasicUserDetailsDB();

        String id = userDto.getLoginDto().getMd5();
        basicUserDetailsDB.setId(id);
        basicUserDetailsDB.setDateRegistered(userDto.getRegistered());
        basicUserDetailsDB.setPhone(userDto.getPhone());
        basicUserDetailsDB.setGender(userDto.getGender());

        if (userDto.getLocationDto() != null) {
            LocationDB locationDB = new LocationDB();
            locationDB.setId(id);
            locationDB.setCity(userDto.getLocationDto().getCity());
            locationDB.setState(userDto.getLocationDto().getState());
            locationDB.setStreet(userDto.getLocationDto().getStreet());
            basicUserDetailsDB.setLocation(locationDB);
        }
        if (userDto.getPictureDto() != null) {
            PhotoDB photoDB = new PhotoDB();
            photoDB.setId(id);
            photoDB.setLarge(userDto.getPictureDto().getLarge());
            photoDB.setMedium(userDto.getPictureDto().getMedium());
            photoDB.setThumbnail(userDto.getPictureDto().getThumbnail());
            basicUserDetailsDB.setPhoto(photoDB);
        }

        return basicUserDetailsDB;
    }


}
