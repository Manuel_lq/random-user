
package com.mlcorrea.randomuser.data.network.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponseDto {

    @SerializedName("results")
    @Expose
    private List<UserDto> userList = null;
    @SerializedName("info")
    @Expose
    private InfoDto infoDto;

    public List<UserDto> getUserList() {
        return userList;
    }

    public void setUserList(List<UserDto> userList) {
        this.userList = userList;
    }

    public InfoDto getInfoDto() {
        return infoDto;
    }

    public void setInfoDto(InfoDto infoDto) {
        this.infoDto = infoDto;
    }

}
