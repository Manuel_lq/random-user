package com.mlcorrea.randomuser.commonviews;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by manuel on 11/06/17.
 */

public class ImageViewSquared extends AppCompatImageView {

    public ImageViewSquared(Context context) {
        super(context);
    }

    public ImageViewSquared(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewSquared(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}
