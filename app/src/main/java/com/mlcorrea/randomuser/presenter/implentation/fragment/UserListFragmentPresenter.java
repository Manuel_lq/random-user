package com.mlcorrea.randomuser.presenter.implentation.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.eventbus.BusUser;
import com.mlcorrea.randomuser.business.interactor.UserCases;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.business.repository.UserRepository;
import com.mlcorrea.randomuser.presenter.base.PresenterBase;
import com.mlcorrea.randomuser.ui.adapter.base.StatesRecyclerViewAdapter;
import com.mlcorrea.randomuser.ui.contract.fragment.UserListFragmentContract;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

import static com.mlcorrea.randomuser.business.eventbus.BusUser.TypeBusUser.ON_ERROR;

/**
 * Created by manuel on 10/06/17.
 */

public class UserListFragmentPresenter extends PresenterBase<UserListFragmentContract.View> implements UserListFragmentContract.Presenter {


    private static final String TAG = UserListFragmentPresenter.class.getSimpleName();
    private static final String GET_USER_LIST_API = TAG + "_API_GET_USER_LIST";

    private static final int DEFAULT_PAGE_INDEX = 1;
    private static final int PAGINATION_SIZE = 10;

    private final UserCases userCases;
    private final UserRepository userRepository;
    private final EventBus eventBus;

    private UserRepository.UserCallback userCallback;
    private int pageList = DEFAULT_PAGE_INDEX;
    private boolean isFilterEnable;

    @Inject
    public UserListFragmentPresenter(UserCases userCases, UserRepository userRepository, EventBus eventBus) {
        this.userCases = userCases;
        this.userRepository = userRepository;
        this.eventBus = eventBus;

        initListener();
    }

    @Override
    public void onViewCreate() {
        super.onViewCreate();
        if (!this.eventBus.isRegistered(this)) {
            this.eventBus.register(this);
        }
    }

    @Override
    public void onViewStart() {
        super.onViewStart();
        //add DB listener
        this.userRepository.addListerUserTable(this.userCallback, TAG);
    }

    @Override
    public void onViewStop() {
        super.onViewStop();
        //Remove DB listener
        this.userRepository.removeListenerUserTable(this.userCallback);
    }

    @Override
    public void onViewDestroy() {
        super.onViewDestroy();
        if (this.eventBus.isRegistered(this)) {
            this.eventBus.unregister(this);
        }
    }

    @Override
    public void refreshUserList() {
        if (view == null) {
            return;
        }
        this.view.resetScrollView();
        getUserListPage(DEFAULT_PAGE_INDEX);
    }

    @Override
    public void getUserListPage(int page) {
        this.pageList = page;
        this.userCases.getUserList(this.pageList, PAGINATION_SIZE, TAG, GET_USER_LIST_API);
    }

    @Override
    public void deleteUser(@NonNull User user) {
        this.userRepository.deleteUserFromDB(user.getId());
    }

    @Override
    public void addUserFilter(@Nullable String filter) {
        isFilterEnable = !StringUtils.isEmpty(filter);
        //we remove the DB listener if the filter is enable
        if (isFilterEnable) {
            this.userRepository.removeListenerUserTable(this.userCallback);
            filterUserList(filter);
        } else {//add the DB listener if the filter is disable
            this.userRepository.addListerUserTable(this.userCallback, TAG);
        }

    }

    @Override
    public void openUserDetails(@NonNull User user) {
        if (this.view == null) {
            return;
        }
        this.view.openUserDetailActivity(user);
    }

    /*---------------------PRIVATE METHOD-----------------*/

    /**
     * Filter the DB and populate the adapter
     *
     * @param filter {@link String}
     */
    private void filterUserList(@NonNull String filter) {
        if (this.view == null) {
            return;
        }
        List<User> userList = this.userRepository.getFilterUserList(filter);
        if (userList == null || userList.isEmpty()) {
            this.view.setAdapterState(StatesRecyclerViewAdapter.STATE_EMPTY);
            return;
        }
        this.view.setAdapter(userList);
        this.view.setAdapterState(StatesRecyclerViewAdapter.STATE_NORMAL);
    }

    /**
     * Populate the adapter with the data from the DB
     *
     * @param userList {@link List<User>}
     */
    private void setListToAdapter(@Nullable List<User> userList) {
        if (this.view == null) {
            return;
        }

        if (userList == null || userList.isEmpty()) {
            this.view.setAdapterState(StatesRecyclerViewAdapter.STATE_EMPTY);
            return;
        }

        //full list user
        int size = userList.size();
        //number of user expected
        int numberJob = this.pageList * PAGINATION_SIZE;
        int maxLength = size < numberJob ? size : numberJob;

        List<User> subList = userList.subList(0, maxLength);
        this.view.setAdapter(subList);
        this.view.setAdapterState(StatesRecyclerViewAdapter.STATE_NORMAL);
    }

    /*------------------------LISTENER---------------------*/

    private void initListener() {
        //DB listener to User table
        this.userCallback = new UserRepository.UserCallback() {
            @Override
            public void userList(List<User> userList) {
                Timber.d("Success User List");
                setListToAdapter(userList);
            }

            @Override
            public void onError(@NonNull Exception e) {
                Timber.d("Error User List");
            }
        };
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventMainThread(BusUser event) {
        if (!StringUtils.equals(event.tag, GET_USER_LIST_API)) {
            return;
        }
        if (view == null) {
            return;
        }
        view.showPullToRefresh(false);
        if (event.typeBusUser == ON_ERROR) {
            //Display error
            if (event.object instanceof Exception)
                view.showSnackBarError((Exception) event.object);
        }
    }

}
