package com.mlcorrea.randomuser.presenter.base;

import android.support.annotation.NonNull;

/**
 * Created by manuel on 10/06/17.
 *
 * Basic interfaces to simulate the lifecycle activity / fragment
 */

public interface BasePresenter {

    void setView(@NonNull BaseView view);

    void onViewCreate();

    void onViewStart();

    void onViewResume();

    void onViewPause();

    void onViewStop();

    void onViewDestroy();

    void onActivityDetached();
}
