package com.mlcorrea.randomuser.presenter.implentation.activity;

import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.presenter.base.PresenterBase;
import com.mlcorrea.randomuser.ui.contract.activity.MainActivityContract;

import javax.inject.Inject;

/**
 * Created by manuel on 11/06/17.
 */

public class MainActivityPresenter extends PresenterBase<MainActivityContract.View> implements MainActivityContract.Presenter {

    @Inject
    public MainActivityPresenter() {
    }

    @Override
    public void setUserFilter(@Nullable String filter) {
        if (this.view==null){
            return;
        }
        this.view.addUserFilter(filter);
    }
}
