package com.mlcorrea.randomuser.presenter.implentation.activity;

import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.business.repository.UserRepository;
import com.mlcorrea.randomuser.presenter.base.PresenterBase;
import com.mlcorrea.randomuser.ui.contract.activity.UserDetailsActivityContract;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

/**
 * Created by manuel on 11/06/17.
 */

public class UserDetailsActivityPresenter extends PresenterBase<UserDetailsActivityContract.View> implements UserDetailsActivityContract.Presenter {

    private static final String MALE = "male";
    private final UserRepository userRepository;

    @Inject
    public UserDetailsActivityPresenter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void setUserData(@Nullable String userId) {
        if (StringUtils.isEmpty(userId)) {

            return;
        }
        User user = this.userRepository.getUserById(userId);
        if (user == null) {
            return;
        }
        if (this.view == null) {
            return;
        }
        this.view.updateUI(user);
        //this could be done much better with a enum
        this.view.setGender(StringUtils.equals(user.getGender(), MALE));
        this.view.setUserDetailsFragment(user);
    }
}
