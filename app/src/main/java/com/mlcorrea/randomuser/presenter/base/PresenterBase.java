package com.mlcorrea.randomuser.presenter.base;

import android.support.annotation.NonNull;

/**
 * Created by manuel on 10/06/17.
 */

public abstract class PresenterBase<T extends BaseView> implements BasePresenter {

    protected T view;

    @SuppressWarnings("unchecked")
    @Override
    public void setView(@NonNull BaseView view) {
        this.view = (T) view;
    }

    @Override
    public void onViewCreate() {

    }

    @Override
    public void onViewStart() {

    }

    @Override
    public void onViewResume() {

    }

    @Override
    public void onViewPause() {

    }

    @Override
    public void onViewStop() {

    }

    @Override
    public void onViewDestroy() {

    }

    @Override
    public void onActivityDetached() {

    }
}
