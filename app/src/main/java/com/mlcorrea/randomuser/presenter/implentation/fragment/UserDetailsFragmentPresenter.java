package com.mlcorrea.randomuser.presenter.implentation.fragment;

import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.presenter.base.PresenterBase;
import com.mlcorrea.randomuser.ui.contract.fragment.UserDetailsFragmentContract;

import javax.inject.Inject;

/**
 * Created by manuel on 11/06/17.
 */

public class UserDetailsFragmentPresenter extends PresenterBase<UserDetailsFragmentContract.View> implements UserDetailsFragmentContract.Presenter {

    @Inject
    public UserDetailsFragmentPresenter() {
    }

    @Override
    public void setUserData(@Nullable User userData) {
        if (this.view==null){
            return;
        }
        this.view.updateUI(userData);
    }
}
