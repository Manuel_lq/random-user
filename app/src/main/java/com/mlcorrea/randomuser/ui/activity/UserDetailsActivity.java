package com.mlcorrea.randomuser.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.di.HasComponent;
import com.mlcorrea.randomuser.di.components.activity.DaggerUserDetailsActivityComponent;
import com.mlcorrea.randomuser.di.components.activity.UserDetailsActivityComponent;
import com.mlcorrea.randomuser.di.modules.activity.UserDetailsActivityModule;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.ui.base.ActivityBase;
import com.mlcorrea.randomuser.ui.contract.activity.UserDetailsActivityContract;
import com.mlcorrea.randomuser.ui.fragment.UserDetailsFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class UserDetailsActivity extends ActivityBase implements HasComponent<UserDetailsActivityComponent>, UserDetailsActivityContract.View {

    private static final String INTENT_EXTRA_USER_ID = "INTENT_EXTRA_USER_ID";

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout uiCollapsingToolbar;
    @BindView(R.id.ui_user_photo)
    AppCompatImageView uiUserPhoto;
    @BindView(R.id.ui_gender_btn)
    FloatingActionButton uiGenderBtn;
    @BindView(R.id.toolbar)
    Toolbar uiToolbar;


    @Inject
    UserDetailsActivityContract.Presenter presenter;

    private UserDetailsActivityComponent userDetailsActivityComponent;


    public static Intent createIntent(Context context, @NonNull String userId) {
        Intent intent = new Intent(context, UserDetailsActivity.class);
        intent.putExtra(INTENT_EXTRA_USER_ID, userId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        ButterKnife.bind(this);
        setActionBarStatus();
        if (savedInstanceState == null) {
            Bundle bundle = getIntent().getExtras();
            String userId = bundle.containsKey(INTENT_EXTRA_USER_ID) ? bundle.getString(INTENT_EXTRA_USER_ID) : null;
            this.presenter.setUserData(userId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*---------------------BASE ACTIVITY-----------------*/

    @Nullable
    @Override
    protected BasePresenter getPresenter() {
        return this.presenter;
    }

    @Override
    protected void injectComponentToActivity() {
        //inject the component to the activity
        this.userDetailsActivityComponent = DaggerUserDetailsActivityComponent.builder()
                .androidApplicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .userDetailsActivityModule(new UserDetailsActivityModule())
                .build();
        getComponent().inject(this);
    }

    @Override
    public UserDetailsActivityComponent getComponent() {
        return this.userDetailsActivityComponent;
    }

     /*-------------------------VIEW-------------------*/

    @Override
    public void updateUI(@NonNull User user) {
        this.uiCollapsingToolbar.setTitle(user.getFullName());
        //TODO improve this. add method passing the size, enum or a variant could be nice
        String photoUrl = user.getPhoto() == null ? null : user.getPhoto().getLarge();
        Glide.with(this)
                .load(photoUrl)
                .placeholder(R.drawable.ic_person_black_24dp)
                .centerCrop()
                .bitmapTransform(new CropCircleTransformation(this))
                .into(uiUserPhoto);
    }

    @Override
    public void setGender(boolean isMale) {
        this.uiGenderBtn.setImageResource(isMale ? R.drawable.ic_mars_symbol : R.drawable.ic_female_black_symbol);
    }

    @Override
    public void setUserDetailsFragment(@NonNull User user) {
        UserDetailsFragment userDetailsFragment = UserDetailsFragment.newInstance(user);
        getSupportFragmentManager().beginTransaction().replace(R.id.ui_user_details_container, userDetailsFragment, UserDetailsFragment.class.getSimpleName()).commit();
    }

     /*-------------PRIVATE METHOD-------*/

    private void setActionBarStatus() {
        setSupportActionBar(this.uiToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}
