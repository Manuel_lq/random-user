package com.mlcorrea.randomuser.ui.contract.activity;

import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.presenter.base.BaseView;

/**
 * Created by manuel on 11/06/17.
 */

public interface MainActivityContract {

    interface View extends BaseView {
        /**
         * Send the text to the Fragment to filter the DB and populate the adapter
         *
         * @param filter {@link String }
         */
        void addUserFilter(@Nullable String filter);
    }

    interface Presenter extends BasePresenter {

        /**
         * Send the text from the View
         *
         * @param filter {@link String }
         */
        void setUserFilter(@Nullable String filter);
    }
}
