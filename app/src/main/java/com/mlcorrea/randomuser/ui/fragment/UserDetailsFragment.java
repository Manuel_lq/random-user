package com.mlcorrea.randomuser.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.di.components.activity.UserDetailsActivityComponent;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.ui.base.FragmentBase;
import com.mlcorrea.randomuser.ui.contract.fragment.UserDetailsFragmentContract;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDetailsFragment extends FragmentBase implements UserDetailsFragmentContract.View {

    private static final String ARG_USER = "ARG_USER";

    @BindView(R.id.ui_user_date)
    TextView uiUserDate;
    @BindView(R.id.ui_user_phone)
    TextView uiUserPhone;
    @BindView(R.id.ui_user_email)
    TextView uiUserEmail;

    @Inject
    UserDetailsFragmentContract.Presenter presenter;

    public UserDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param user Parameter 1.
     * @return A new instance of fragment UserDetailsFragment.
     */
    public static UserDetailsFragment newInstance(@NonNull User user) {
        UserDetailsFragment fragment = new UserDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    /*------------------------BASE FRAGMENT---------------*/

    @Override
    protected void injectComponent() {
        this.getComponent(UserDetailsActivityComponent.class).inject(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_user_details;
    }

    @Override
    protected void presenterIsReady(@Nullable Bundle savedInstanceState) {

        if (getArguments().containsKey(ARG_USER)) {
            User user = (User) getArguments().getSerializable(ARG_USER);
            this.presenter.setUserData(user);
        }
    }

    @Nullable
    @Override
    protected BasePresenter getPresenter() {
        return this.presenter;
    }

    @Override
    public void updateUI(@NonNull User user) {
        this.uiUserDate.setText(user.getDateRegistered());
        this.uiUserPhone.setText(user.getPhone());
        this.uiUserEmail.setText(user.getEmail());
    }

     /*--------------------VIEW----------------*/

}
