package com.mlcorrea.randomuser.ui.adapter.base;

import android.content.Context;
import android.support.annotation.IntDef;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by manuel on 11/06/17.
 */

public class StatesRecyclerViewAdapter extends StatestRecyclerViewAdapterWrapper {

    @LayoutRes
    private final int uiLoadingView;
    @LayoutRes
    private final int uiEmptyView;
    @LayoutRes
    private final int uiErrorView;

    @IntDef({STATE_NORMAL, STATE_LOADING, STATE_EMPTY, STATE_ERROR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface State {
    }

    public static final int STATE_NORMAL = 0;
    public static final int STATE_LOADING = 1;
    public static final int STATE_EMPTY = 2;
    public static final int STATE_ERROR = 3;

    private static final int TYPE_LOADING = 0x00020;
    private static final int TYPE_EMPTY = 0x00021;
    private static final int TYPE_ERROR = 0x00022;

    @State
    private int state = STATE_NORMAL;
    private final StateRecycleViewAdapterListener stateRecycleViewAdapterListener;
    private final RecyclerView recyclerView;

    public StatesRecyclerViewAdapter(@NonNull RecyclerView.Adapter wrapped, @LayoutRes int loadingView, @LayoutRes int emptyView, @LayoutRes int errorView, StateRecycleViewAdapterListener stateRecycleViewAdapterListener, RecyclerView recyclerView) {
        super(wrapped);
        this.uiLoadingView = loadingView;
        this.uiEmptyView = emptyView;
        this.uiErrorView = errorView;
        this.stateRecycleViewAdapterListener = stateRecycleViewAdapterListener;
        this.recyclerView = recyclerView;


        final GridLayoutManager layoutManager = (GridLayoutManager) (recyclerView.getLayoutManager());
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                int typePositionAdapter = getItemViewType(position);
                return typePositionAdapter == TYPE_LOADING || typePositionAdapter == TYPE_EMPTY || typePositionAdapter == TYPE_ERROR ? layoutManager.getSpanCount() : 1;
            }
        });
    }

    @State
    public int getState() {
        return state;
    }

    public void setState(@State int state) {
        this.state = state;
        getWrappedAdapter().notifyDataSetChanged();
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        switch (state) {
            case STATE_LOADING:
            case STATE_EMPTY:
            case STATE_ERROR:
                return 1;
        }
        return super.getItemCount();
    }

    @Override
    public int getItemViewType(int position) {
        switch (state) {
            case STATE_LOADING:
                return TYPE_LOADING;
            case STATE_EMPTY:
                return TYPE_EMPTY;
            case STATE_ERROR:
                return TYPE_ERROR;
        }
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        switch (viewType) {
            case TYPE_LOADING:
                View loadingView = LayoutInflater.from(context).inflate(this.uiLoadingView, null);
                loadingView.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return new SimpleViewHolder(loadingView);
            case TYPE_EMPTY:
                View emptyView = LayoutInflater.from(context).inflate(this.uiEmptyView, null);
                emptyView.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return new SimpleViewHolder(emptyView);
            case TYPE_ERROR:
                View errorView = LayoutInflater.from(context).inflate(this.uiErrorView, null);
                errorView.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                return new SimpleViewHolder(errorView);
        }
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (state) {
            case STATE_LOADING:
                this.stateRecycleViewAdapterListener.onBindLoadingViewHolder(holder, position);
                break;
            case STATE_EMPTY:
                this.stateRecycleViewAdapterListener.onBindEmptyViewHolder(holder, position);
                break;
            case STATE_ERROR:
                this.stateRecycleViewAdapterListener.onBindErrorViewHolder(holder, position);
                break;
            default:
                super.onBindViewHolder(holder, position);
                break;
        }
    }


    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public SimpleViewHolder(View itemView) {
            super(itemView);
        }
    }


    public interface StateRecycleViewAdapterListener {
        void onBindErrorViewHolder(RecyclerView.ViewHolder holder, int position);

        void onBindEmptyViewHolder(RecyclerView.ViewHolder holder, int position);

        void onBindLoadingViewHolder(RecyclerView.ViewHolder holder, int position);
    }
}
