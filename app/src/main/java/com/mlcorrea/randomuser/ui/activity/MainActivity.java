package com.mlcorrea.randomuser.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;

import com.jakewharton.rxbinding2.support.v7.widget.RxSearchView;
import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.di.HasComponent;
import com.mlcorrea.randomuser.di.components.activity.DaggerMainActivityComponent;
import com.mlcorrea.randomuser.di.components.activity.MainActivityComponent;
import com.mlcorrea.randomuser.di.modules.activity.MainActivityModule;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.ui.base.ActivityBase;
import com.mlcorrea.randomuser.ui.contract.activity.MainActivityContract;
import com.mlcorrea.randomuser.ui.fragment.UserListFragment;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import timber.log.Timber;

public class MainActivity extends ActivityBase implements HasComponent<MainActivityComponent>, MainActivityContract.View {

    @BindView(R.id.ui_search_bar)
    SearchView uiSearchView;


    @Inject
    MainActivityContract.Presenter presenter;

    private MainActivityComponent mainActivityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.RandomUserTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUI();
        if (savedInstanceState == null) {
            UserListFragment userListFragment = UserListFragment.newInstance();
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, userListFragment, UserListFragment.class.getSimpleName()).commit();
        }
    }

    /*---------------------BASE ACTIVITY-----------------*/

    @Nullable
    @Override
    protected BasePresenter getPresenter() {
        return this.presenter;
    }

    @Override
    protected void injectComponentToActivity() {
        //inject the component to the activity
        this.mainActivityComponent = DaggerMainActivityComponent.builder()
                .androidApplicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .mainActivityModule(new MainActivityModule())
                .build();
        getComponent().inject(this);
    }

    @Override
    public MainActivityComponent getComponent() {
        return this.mainActivityComponent;
    }

    /*-------------------------VIEW-------------------*/

    @Override
    public void addUserFilter(@Nullable final String filter) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_container);
        if (fragment instanceof UserListFragment && fragment.isVisible() && fragment.isResumed()) {
            UserListFragment userListFragment = (UserListFragment) fragment;
            userListFragment.addUserFilter(filter);
        }
    }

    /*-----------------------PRIVATE METHOD------------*/

    private void setUI() {
        RxSearchView.queryTextChanges(uiSearchView)
                // .filter(charSequence -> !StringUtils.isEmpty(charSequence))
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Timber.d("new user filter %s", response.toString());
                    presenter.setUserFilter(response.toString());
                });
    }

}
