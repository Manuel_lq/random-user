package com.mlcorrea.randomuser.ui.contract.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.presenter.base.BaseView;

/**
 * Created by manuel on 11/06/17.
 */

public interface UserDetailsFragmentContract {

    interface View extends BaseView {
        void updateUI(@NonNull User user);
    }

    interface Presenter extends BasePresenter {

        void setUserData(@Nullable User userData);

    }
}
