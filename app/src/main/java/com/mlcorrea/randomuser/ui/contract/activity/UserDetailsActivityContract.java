package com.mlcorrea.randomuser.ui.contract.activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.presenter.base.BaseView;

/**
 * Created by manuel on 11/06/17.
 */

public interface UserDetailsActivityContract {

    interface View extends BaseView {

        /**
         * Update the UI with the user data
         *
         * @param user
         */
        void updateUI(@NonNull User user);

        /**
         * set the gender to the UI
         *
         * @param isMale
         */
        void setGender(boolean isMale);

        /**
         * open the user data fragment
         *
         * @param user
         */
        void setUserDetailsFragment(@NonNull User user);

    }

    interface Presenter extends BasePresenter {

        /**
         * Update the presenter with the user data
         *
         * @param userId
         */
        void setUserData(@Nullable String userId);
    }
}
