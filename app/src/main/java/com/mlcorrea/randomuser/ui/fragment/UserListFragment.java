package com.mlcorrea.randomuser.ui.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.di.components.activity.MainActivityComponent;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.ui.activity.UserDetailsActivity;
import com.mlcorrea.randomuser.ui.adapter.UserListAdapter;
import com.mlcorrea.randomuser.ui.adapter.base.StatesRecyclerViewAdapter;
import com.mlcorrea.randomuser.ui.base.FragmentBase;
import com.mlcorrea.randomuser.ui.contract.fragment.UserListFragmentContract;
import com.mlcorrea.randomuser.utils.BusinessConstants;
import com.mlcorrea.randomuser.utils.recyclerview.EndlessRecyclerViewScrollListener;
import com.mlcorrea.randomuser.utils.recyclerview.GridSpacingItemDecoration;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserListFragment extends FragmentBase implements UserListFragmentContract.View, SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.ui_swipe_refresh)
    SwipeRefreshLayout uiSwipeRefresh;
    @BindView(R.id.ui_recycler_view_users)
    RecyclerView uiRecyclerView;

    @Inject
    UserListFragmentContract.Presenter presenter;

    private UserListAdapter userListAdapter;
    private StatesRecyclerViewAdapter statesRecyclerViewAdapter;
    private UserListAdapter.UserListAdapterListener userListAdapterListener;
    private EndlessRecyclerViewScrollListener scrollListener;


    public UserListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment UserListFragment.
     */
    public static UserListFragment newInstance() {
        return new UserListFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initListener();
    }

    /*------------------------BASE FRAGMENT---------------*/

    @Override
    protected void injectComponent() {
        this.getComponent(MainActivityComponent.class).inject(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_user_list;
    }

    @Override
    protected void presenterIsReady(@Nullable Bundle savedInstanceState) {
        setUI();
        this.presenter.refreshUserList();
    }

    @Nullable
    @Override
    protected BasePresenter getPresenter() {
        return this.presenter;
    }

     /*--------------------VIEW----------------*/

    @Override
    public void setAdapterState(@StatesRecyclerViewAdapter.State int adapterState) {
        this.statesRecyclerViewAdapter.setState(adapterState);
    }

    @Override
    public void setAdapter(@Nullable List<User> itemList) {
        this.userListAdapter.addNewList(itemList);
    }

    @Override
    public void showSnackBarError(@Nullable Exception e) {
        showSnackBar(getErrorMessage(e));
    }

    @Override
    public void showPullToRefresh(final boolean state) {
        this.uiSwipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                Timber.d("setProgressBarLoadingState," + "shouldBeLoading : " + state);
                if (UserListFragment.this.uiSwipeRefresh != null) {
                    UserListFragment.this.uiSwipeRefresh.setRefreshing(state);
                }
            }
        });
    }

    @Override
    public void resetScrollView() {
        this.scrollListener.resetState();
    }

    @Override
    public void openUserDetailActivity(@NonNull User user) {
        Intent intent = UserDetailsActivity.createIntent(getContext(), user.getId());
        startActivity(intent);
    }

     /*--------------------------------REFRESH CALL BACK----------------------*/

    @Override
    public void onRefresh() {
        Timber.d("onRefresh");
        this.presenter.refreshUserList();
    }

    /*-----------------------PUBLIC METHOD-------------------*/

    public void addUserFilter(@Nullable String filter) {
        this.presenter.addUserFilter(filter);
    }

    /*------------------------PRIVATE METHOD-----------------*/

    private void setUI() {
        this.uiSwipeRefresh.setOnRefreshListener(this);
        this.uiSwipeRefresh.setColorSchemeResources(BusinessConstants.SWIPE_TO_REFRESH_COLORS);

        //initializing the adapter list
        this.userListAdapter = new UserListAdapter();
        this.userListAdapter.setUserListAdapertListener(this.userListAdapterListener);

        //initializing recycler view
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.columns_of_users));
        this.uiRecyclerView.setLayoutManager(gridLayoutManager);
        this.uiRecyclerView.addItemDecoration(new GridSpacingItemDecoration(getResources().getInteger(R.integer.columns_of_users), 18, true, 0));
        this.uiRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.uiRecyclerView.setHasFixedSize(true);

        //adding different states for the adapter
        this.statesRecyclerViewAdapter = new StatesRecyclerViewAdapter(this.userListAdapter, R.layout.layout_loading_state, R.layout.layout_empty_state, R.layout.layout_empty_state, new StatesRecyclerViewAdapter.StateRecycleViewAdapterListener() {
            @Override
            public void onBindErrorViewHolder(RecyclerView.ViewHolder holder, int position) {
                //Find the view id here if the view as a button
            }

            @Override
            public void onBindEmptyViewHolder(RecyclerView.ViewHolder holder, int position) {
                //Find the id here if the view as a button
            }

            @Override
            public void onBindLoadingViewHolder(RecyclerView.ViewHolder holder, int position) {
                //Find the id here if the view as a button
            }
        }, uiRecyclerView);

        this.uiRecyclerView.setAdapter(this.statesRecyclerViewAdapter);
        //set initial state.
        this.statesRecyclerViewAdapter.setState(StatesRecyclerViewAdapter.STATE_LOADING);

        //setting a listener for the recycler view
        this.scrollListener = new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                Timber.d("Page number: %s", page + 1);
                //we increment the page because it already have one
                onLoadMoreUsers(page + 1);
            }
        };
        this.uiRecyclerView.addOnScrollListener(this.scrollListener);
    }

    private void onLoadMoreUsers(int page) {
        this.presenter.getUserListPage(page);
    }

    /*-----------------------LISTENER---------------------*/

    private void initListener() {
        this.userListAdapterListener = new UserListAdapter.UserListAdapterListener() {
            @Override
            public void onDeleteUser(@NonNull User user) {
                presenter.deleteUser(user);
            }

            @Override
            public void onCardClicked(@NonNull User user) {
                presenter.openUserDetails(user);
            }
        };
    }
}
