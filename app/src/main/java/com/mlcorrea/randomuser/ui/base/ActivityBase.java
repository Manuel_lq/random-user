package com.mlcorrea.randomuser.ui.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;

import com.mlcorrea.randomuser.RandomUserApplication;
import com.mlcorrea.randomuser.di.components.AndroidApplicationComponent;
import com.mlcorrea.randomuser.di.modules.ActivityModule;
import com.mlcorrea.randomuser.errors.ErrorMessageCreator;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.presenter.base.BaseView;

import javax.inject.Inject;

import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public abstract class ActivityBase extends AppCompatActivity implements BaseView {


    @Inject
    ErrorMessageCreator errorMessageCreator;

    /*------ abstract methods ------*/

    /**
     * @return Presenter associated with this View
     */
    @Nullable
    protected abstract BasePresenter getPresenter();
    protected abstract void injectComponentToActivity();

    /*------ abstract methods ------*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getApplicationComponent().inject(this);
        // injectViews();
        injectComponentToActivity();
        //set the view to the presenter
        if (getPresenter() != null) {
            getPresenter().setView(this);
        }
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewCreate();
        }
    }

    @Override
    protected void onStart() {
        Timber.d("onStart");
        super.onStart();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewStart();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewPause();
        }
    }

    @Override
    protected void onStop() {
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewStop();
        }
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewDestroy();
        }
    }

    private void injectViews() {
        ButterKnife.bind(this);
    }

      /*------------------------PUBLIC METHOD---------------------*/

    protected AndroidApplicationComponent getApplicationComponent() {
        return ((RandomUserApplication) getApplication()).getAndroidApplicationComponent();
    }

    /**
     * Get an Activity module for dependency injection.
     *
     * @return {@link ActivityModule}
     */
    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }


    public void gotoActivity(Class activityClassReference) {
        Intent i = new Intent(this, activityClassReference);
        startActivity(i);
    }

    /**
     * get the error message
     * @param exception {@link Exception}
     * @return {@link String }
     */
    @NonNull
    public String getMessageError(@Nullable Exception exception) {
        return this.errorMessageCreator.getMessageFromException(exception);
    }

    /**
     * Display the error message in a snack bar
     *
     * @param exception  {@link Exception}
     */
    public void showErrorMessage(@Nullable Exception exception) {
        String errorMessage = getMessageError(exception);

        showSnackBar(errorMessage);
    }

    /**
     * Display a snack bar
     *
     * @param message {@link String }
     */
    public void showSnackBar(@Nullable String message) {
        if (TextUtils.isEmpty(message)) {
            return;
        }
        View view = findViewById(android.R.id.content);
        if (view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
        }
    }
}
