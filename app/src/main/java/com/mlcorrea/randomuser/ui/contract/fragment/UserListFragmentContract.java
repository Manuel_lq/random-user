package com.mlcorrea.randomuser.ui.contract.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.presenter.base.BaseView;
import com.mlcorrea.randomuser.ui.adapter.base.StatesRecyclerViewAdapter;

import java.util.List;

/**
 * Created by manuel on 10/06/17.
 */

public interface UserListFragmentContract {

    interface View extends BaseView {

        /**
         * Change the status of the adapter
         *
         * @param adapterState {@link Integer}
         */
        void setAdapterState(@StatesRecyclerViewAdapter.State int adapterState);

        /**
         * Set the adapter
         *
         * @param itemList {@link List<User>}
         */
        void setAdapter(@Nullable List<User> itemList);

        /**
         * Display a snack bar in case of error
         *
         * @param e {@link Exception}
         */
        void showSnackBarError(@Nullable Exception e);

        /**
         * Change the visibility of the pull to refresh#
         *
         * @param state {@link Boolean}
         */
        void showPullToRefresh(final boolean state);

        /**
         * Reset infinite scroll view
         */
        void resetScrollView();

        /**
         * Open User detail Activity
         *
         * @param user {@link User}
         */
        void openUserDetailActivity(@NonNull User user);
    }

    interface Presenter extends BasePresenter {

        /**
         * Force the list to refresh
         */
        void refreshUserList();

        /**
         * Fetch the next page from network
         *
         * @param page {@link Integer}
         */
        void getUserListPage(int page);

        /**
         * Mark the user as delete
         *
         * @param user {@link User}
         */
        void deleteUser(@NonNull User user);

        /**
         * Filter the user DB for first name, second name and email
         *
         * @param filter {@link String}
         */
        void addUserFilter(@Nullable String filter);

        /**
         * Open full user details activity
         *
         * @param user {@link User}
         */
        void openUserDetails(@NonNull User user);
    }

}
