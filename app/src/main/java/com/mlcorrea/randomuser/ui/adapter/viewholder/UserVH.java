package com.mlcorrea.randomuser.ui.adapter.viewholder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.commonviews.ImageViewSquared;
import com.mlcorrea.randomuser.ui.adapter.UserListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by manuel on 11/06/17.
 */

public class UserVH extends RecyclerView.ViewHolder {

    @BindView(R.id.ui_user_email)
    TextView uiUserEmail;
    @BindView(R.id.ui_user_phone)
    TextView uiUserPhone;
    @BindView(R.id.ui_user_name)
    TextView uiUserName;
    @BindView(R.id.ui_user_photo)
    ImageViewSquared uiUserPhoto;
    @BindView(R.id.ui_menu)
    AppCompatImageView uiMenu;

    @Nullable
    private final UserListAdapter.UserListAdapterListener userListAdapterListener;

    public UserVH(View itemView, @Nullable UserListAdapter.UserListAdapterListener userListAdapterListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.userListAdapterListener = userListAdapterListener;
    }

    /**
     * Set the data to the UI
     *
     * @param user {@link User}
     */
    public void onBind(@NonNull User user) {
        this.itemView.setTag(user);
        uiUserEmail.setText(user.getEmail());
        uiUserPhone.setText(user.getPhone());
        uiUserName.setText(user.getFullName());

        String photoUrl = user.getPhoto() == null ? null : user.getPhoto().getLarge();

        Glide.with(this.itemView.getContext())
                .load(photoUrl)
                .asBitmap()
                .placeholder(R.color.gray_dark)
                .into(uiUserPhoto);
    }

    @SuppressWarnings("unused")
    @OnClick({R.id.ui_menu,R.id.ui_user_card_container})
    public void onBtnClicked(View button) {
        switch (button.getId()) {
            case R.id.ui_menu:
                showPopupMenu(button);
                break;
            case R.id.ui_user_card_container:
                User user = (User) this.itemView.getTag();
                if (this.userListAdapterListener != null) {
                    this.userListAdapterListener.onCardClicked(user);
                }
                break;

        }
    }

    /*-----------------PRIVATE METHOD-------------*/

    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this.itemView.getContext(), v);
        popupMenu.getMenuInflater().inflate(R.menu.user_card_menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {
                deleteUser(item);
                return true;
            }
        });

        popupMenu.show();
    }

    private void deleteUser(MenuItem item) {
        if (item.getItemId() == R.id.menu_action_delete_user) {
            User user = (User) this.itemView.getTag();
            if (this.userListAdapterListener != null) {
                this.userListAdapterListener.onDeleteUser(user);
            }
        }
    }
}
