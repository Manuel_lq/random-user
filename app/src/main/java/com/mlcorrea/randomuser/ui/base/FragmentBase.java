package com.mlcorrea.randomuser.ui.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mlcorrea.randomuser.di.HasComponent;
import com.mlcorrea.randomuser.presenter.base.BasePresenter;
import com.mlcorrea.randomuser.presenter.base.BaseView;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public  abstract class FragmentBase extends Fragment implements IChildContract, BaseView {


    /*------ abstract methods ------*/
    protected abstract void injectComponent();

    protected abstract int getFragmentLayout();

    protected abstract void presenterIsReady(@Nullable Bundle savedInstanceState);

    /**
     * @return Presenter associated with this View
     */
    @Nullable
    protected abstract BasePresenter getPresenter();

    /*------------*/

    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getFragmentLayout(), container, false);
        injectViews(view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Timber.d("onActivityCreated");

        injectComponent();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().setView(this);
        }
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewCreate();
        }

        presenterIsReady(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewStart();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewPause();
        }
    }

    @Override
    public void onStop() {
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewStop();
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onViewDestroy();
        }
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        //notify presenter activity lifecycle
        if (getPresenter() != null) {
            getPresenter().onActivityDetached();
        }
        super.onDetach();
    }

    private void injectViews(final View view) {
        unbinder = ButterKnife.bind(this, view);
    }

    /*------------------------PUBLIC METHOD---------------------*/
    @NonNull
    public String getErrorMessage(@Nullable Exception exception) {
        return ((ActivityBase) getActivity()).getMessageError(exception);
    }

    public void showSnackBar(@NonNull String messageDialog) {
        View view = getView();
        if (view != null) {
            Snackbar.make(view, messageDialog, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected <C> C getComponent(Class<C> componentType) {
        return componentType.cast(((HasComponent<C>) getActivity()).getComponent());
    }
}
