package com.mlcorrea.randomuser.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.business.model.User;
import com.mlcorrea.randomuser.ui.adapter.viewholder.UserVH;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuel on 11/06/17.
 */

public class UserListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface UserListAdapterListener {
        void onDeleteUser(@NonNull User user);

        void onCardClicked(@NonNull User user);
    }

    private List<User> itemList;
    @Nullable
    private UserListAdapterListener userListAdapterListener;

    public UserListAdapter() {
        this.itemList = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_user_card, parent, false);
        return new UserVH(view, this.userListAdapterListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserVH) {
            ((UserVH) holder).onBind(itemList.get(position));
        } else {
            throw new IllegalArgumentException("View holder is not declared in the adapter");
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    /*---------------------PUBLIC METHOD---------------*/

    public void setUserListAdapertListener(@NonNull UserListAdapterListener userListAdapertListener) {
        this.userListAdapterListener = userListAdapertListener;
    }

    /**
     * Add new list to the adapter.
     * <p>
     * it will clear the list and add the new one
     *
     * @param newUserList {@link List<User>}
     */
    public void addNewList(@Nullable List<User> newUserList) {
        this.itemList.clear();
        if (newUserList != null) {
            this.itemList.addAll(newUserList);
        }
        notifyDataSetChanged();
    }
}
