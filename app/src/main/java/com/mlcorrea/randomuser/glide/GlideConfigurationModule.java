package com.mlcorrea.randomuser.glide;

import android.content.Context;
import android.os.Environment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.ExternalCacheDiskCacheFactory;
import com.bumptech.glide.module.GlideModule;

import timber.log.Timber;

/**
 * Created by manuel on 11/06/17.
 */

public class GlideConfigurationModule implements GlideModule {

    private static final String CACHE_FOLDER = "ImagesCache";
    private static final int CACHE_SIZE_CACHE = 104857600;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {

        String downloadDirectoryPath = Environment.getDownloadCacheDirectory().getPath();

        Timber.d(downloadDirectoryPath);
        builder.setDiskCache(new ExternalCacheDiskCacheFactory(context, CACHE_FOLDER, CACHE_SIZE_CACHE));
        //builder.setDecodeFormat(DecodeFormat.PREFER_ARGB_8888);
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }
}
