package com.mlcorrea.randomuser.utils.logger;

import android.util.Log;

import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public class LifeTree extends Timber.Tree {
    private static final String CRASHLYTICS_KEY_PRIORITY = "priority";
    private static final String CRASHLYTICS_KEY_TAG = "tag";
    private static final String CRASHLYTICS_KEY_MESSAGE = "message";

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {

        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return;
        }

        //TODO send the log to where ever you want

    }
}
