package com.mlcorrea.randomuser.utils;

import android.os.Build;

import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public class UtilsDevice {

    public static int getNumberOfCores() {
        try {
            if (Build.VERSION.SDK_INT >= 17) {
                return Runtime.getRuntime().availableProcessors();
            }
        } catch (Exception e) {
            Timber.e(e, "Error getting the number of cores");
        }
        return 4;
    }
}
