package com.mlcorrea.randomuser.utils.logger;

import com.birbit.android.jobqueue.log.CustomLogger;

import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public class JobQueueLogger {
    private static final String TAG_JOB = "[JOB QUEUE]";

    /**
     * Logger for Job queue
     *
     * @return {@link CustomLogger}
     */
    public static CustomLogger getJobLogger() {
        return instance;
    }

    private static final CustomLogger instance = new CustomLogger() {
        @Override
        public boolean isDebugEnabled() {
            return true;
        }

        @Override
        public void d(String text, Object... args) {
            Timber.tag(TAG_JOB).d(text, args);
        }

        @Override
        public void e(Throwable t, String text, Object... args) {
            Timber.tag(TAG_JOB).e(t, text, args);
        }

        @Override
        public void e(String text, Object... args) {
            Timber.tag(TAG_JOB).d(text, args);
        }

        @Override
        public void v(String text, Object... args) {
            Timber.tag(TAG_JOB).v(text, args);
        }
    };
}
