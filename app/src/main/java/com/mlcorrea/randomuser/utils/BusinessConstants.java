package com.mlcorrea.randomuser.utils;

/**
 * Created by manuel on 11/06/17.
 */

public class BusinessConstants {

    public static final int[] SWIPE_TO_REFRESH_COLORS = {android.R.color.holo_blue_bright, android.R.color.holo_green_light, android.R.color.holo_orange_light, android.R.color.holo_red_light};

}
