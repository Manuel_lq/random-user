package com.mlcorrea.randomuser.errors.implementation;

import android.content.Context;
import android.support.annotation.NonNull;

import com.mlcorrea.randomuser.R;
import com.mlcorrea.randomuser.business.contract.ErrorMessageStrings;

import javax.inject.Inject;

/**
 * Created by manuel on 10/06/17.
 */

public class ErrorMessageStringsImpl implements ErrorMessageStrings {

    private final Context context;

    @Inject
    public ErrorMessageStringsImpl(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public String getGenericErrorMessage() {
        return this.context.getResources().getString(R.string.exception_message_generic);
    }

    @NonNull
    @Override
    public String getMessageNotInternetConnection() {
        return this.context.getResources().getString(R.string.exception_message_no_connection);
    }

    @NonNull
    @Override
    public String getMessageErrorParsingResponse() {
        return this.context.getResources().getString(R.string.exception_message_unexpected_error_api);
    }

    @NonNull
    @Override
    public String getMessageErrorEmptyResponse() {
        return this.context.getResources().getString(R.string.exception_message_empty);
    }
}
