package com.mlcorrea.randomuser.errors.implementation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.mlcorrea.randomuser.business.contract.ErrorMessageStrings;
import com.mlcorrea.randomuser.business.model.error.ErrorsModel;
import com.mlcorrea.randomuser.errors.ErrorMapperStrings;
import com.mlcorrea.randomuser.errors.ErrorMessageCreator;

import javax.inject.Inject;

import timber.log.Timber;

import static android.R.id.message;

/**
 * Created by manuel on 10/06/17.
 */

public class ErrorMessageCreatorImpl implements ErrorMessageCreator {

    private final ErrorMessageStrings errorMessageStrings;
    private final ErrorMapperStrings errorMapperStrings;

    @Inject
    public ErrorMessageCreatorImpl(ErrorMessageStrings errorMessageStrings, ErrorMapperStrings errorMapperStrings) {
        this.errorMessageStrings = errorMessageStrings;
        this.errorMapperStrings = errorMapperStrings;
    }


    @Override
    public String getMessageFromException(@Nullable Exception exception) {
        Timber.d("Extract error message from Exception");
        if (exception instanceof ErrorsModel) {
            return getErrorMessageFromErrorsModel((ErrorsModel) exception);
        } else {
            Timber.i("No match for this exception, printing generic error");
            return this.errorMessageStrings.getGenericErrorMessage();
        }
    }

    @Override
    public String getErrorMessageFromErrorsModel(@NonNull ErrorsModel errorsModel) {
        Timber.i("Extract error message from ErrorsModel");
        switch (errorsModel.getRedCodeHTTP()) {
            case ErrorsModel.ERROR_NO_NETWORK:
                Timber.w("Error code: " + ErrorsModel.ERROR_NO_NETWORK + " Error message: " + message);
                return this.errorMessageStrings.getMessageNotInternetConnection();
            case ErrorsModel.ERROR_PARSING_JSON:
                Timber.w("Error code: " + ErrorsModel.ERROR_PARSING_JSON + " Error message: error parsing the JSON");
                return this.errorMessageStrings.getMessageErrorParsingResponse();
            case ErrorsModel.ERROR_EMPTY_RESPONSE:
                Timber.w("Error code: " + ErrorsModel.ERROR_EMPTY_RESPONSE + " Error message: error parsing the JSON");
                return this.errorMessageStrings.getMessageErrorEmptyResponse();

            default:
                String message = TextUtils.isEmpty(errorsModel.getErrorMessage()) ? this.errorMessageStrings.getGenericErrorMessage() : this.errorMapperStrings.mapperErrorModel(errorsModel);
                Timber.w("Error message: " + message);
                return message;
        }
    }
}
