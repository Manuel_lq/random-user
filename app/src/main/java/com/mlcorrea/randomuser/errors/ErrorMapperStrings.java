package com.mlcorrea.randomuser.errors;

import android.support.annotation.NonNull;

import com.mlcorrea.randomuser.business.model.error.ErrorsModel;

/**
 * Created by manuel on 10/06/17.
 */

public interface ErrorMapperStrings {

    String mapperErrorModel(@NonNull ErrorsModel errorModel);
}
