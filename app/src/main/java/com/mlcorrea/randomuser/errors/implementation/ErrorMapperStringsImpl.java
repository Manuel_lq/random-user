package com.mlcorrea.randomuser.errors.implementation;

import android.support.annotation.NonNull;

import com.mlcorrea.randomuser.business.contract.ErrorMessageStrings;
import com.mlcorrea.randomuser.business.model.error.ErrorsModel;
import com.mlcorrea.randomuser.errors.ErrorMapperStrings;

import javax.inject.Inject;

/**
 * Created by manuel on 10/06/17.
 */

public class ErrorMapperStringsImpl implements ErrorMapperStrings {

    private final ErrorMessageStrings errorMessageStrings;

    @Inject
    public ErrorMapperStringsImpl(ErrorMessageStrings errorMessageStrings) {
        this.errorMessageStrings = errorMessageStrings;
    }

    @Override
    public String mapperErrorModel(@NonNull ErrorsModel errorModel) {
        switch (errorModel.getErrorCode()) {

            default:
                return errorModel.getMessage();
        }
    }

}
