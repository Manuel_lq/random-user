package com.mlcorrea.randomuser.errors;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mlcorrea.randomuser.business.model.error.ErrorsModel;

/**
 * Created by manuel on 10/06/17.
 */

public interface ErrorMessageCreator {

    String getMessageFromException(@Nullable Exception exception);

    String getErrorMessageFromErrorsModel(@NonNull ErrorsModel errorsModel);
}
