package com.mlcorrea.randomuser;

import android.app.Application;

import com.mlcorrea.randomuser.business.di.InjectClassToComponent;
import com.mlcorrea.randomuser.business.di.Injector;
import com.mlcorrea.randomuser.data.job.request.GetListUserJob;
import com.mlcorrea.randomuser.di.components.AndroidApplicationComponent;
import com.mlcorrea.randomuser.di.components.DaggerAndroidApplicationComponent;
import com.mlcorrea.randomuser.di.modules.AndroidApplicationModule;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public abstract class BaseApplication extends Application implements Injector {

    protected abstract RandomUserApplication getRandomUserApplication();

    private AndroidApplicationComponent androidApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        initRealm();

        injectApplicationComponent();
    }

    @Override
    public void injectClass(InjectClassToComponent injectClassToComponent) {
        if (injectClassToComponent instanceof GetListUserJob) {
            androidApplicationComponent.inject((GetListUserJob) injectClassToComponent);
        } else {
            Timber.e("You need to declare this class inside of the Base application :%s", injectClassToComponent.getClass().getSimpleName());
            throw new IllegalArgumentException("You need to declare this class inside of the Base application ");
        }
    }

      /*-----------------------PUBLIC METHOD-----------------*/

    public AndroidApplicationComponent getAndroidApplicationComponent() {
        return androidApplicationComponent;
    }

      /*-----------------------PRIVATE METHOD-----------------*/

    private void injectApplicationComponent() {
        androidApplicationComponent = DaggerAndroidApplicationComponent.builder()
                .androidApplicationModule(new AndroidApplicationModule(getRandomUserApplication()))
                .build();

        androidApplicationComponent.inject(this);
    }


    /**
     * Initialize realm BD
     */
    private void initRealm() {
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
