package com.mlcorrea.randomuser.di.components.activity;

import com.mlcorrea.randomuser.di.components.ActivityComponent;
import com.mlcorrea.randomuser.di.components.AndroidApplicationComponent;
import com.mlcorrea.randomuser.di.modules.ActivityModule;
import com.mlcorrea.randomuser.di.modules.activity.MainActivityModule;
import com.mlcorrea.randomuser.di.scope.PerActivity;
import com.mlcorrea.randomuser.ui.activity.MainActivity;
import com.mlcorrea.randomuser.ui.fragment.UserListFragment;

import dagger.Component;

/**
 * Created by manuel on 10/06/17.
 */

@PerActivity
@Component(dependencies = AndroidApplicationComponent.class, modules = {ActivityModule.class, MainActivityModule.class})
public interface MainActivityComponent extends ActivityComponent {

    void inject(MainActivity mainActivity);

    void inject(UserListFragment userListFragment);


}
