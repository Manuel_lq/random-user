package com.mlcorrea.randomuser.di.modules;

import com.mlcorrea.randomuser.business.repository.UserRepository;
import com.mlcorrea.randomuser.data.database.UserModel;
import com.mlcorrea.randomuser.data.database.implentation.UserModelImpl;
import com.mlcorrea.randomuser.data.database.implentation.UserRepositoryImpl;
import com.mlcorrea.randomuser.data.database.wrapper.UserBDWrapper;
import com.mlcorrea.randomuser.data.database.wrapper.implementation.UserDBWrapperImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuel on 10/06/17.
 */
@SuppressWarnings("unused")
@Module
public class DatabaseModule {

    @Provides
    @Singleton
    UserModel provideUserModel(UserModelImpl userModelImpl){
        return userModelImpl;
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(UserRepositoryImpl userRepository){
        return userRepository;
    }

    @Provides
    UserBDWrapper provideUserBDWrapper(UserDBWrapperImpl userDBWrapper){
        return userDBWrapper;
    }

}
