package com.mlcorrea.randomuser.di.modules;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.mlcorrea.randomuser.BuildConfig;
import com.mlcorrea.randomuser.data.network.api.ApiService;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by manuel on 10/06/17.
 */
@SuppressWarnings("unused")
@Module
public class ApiModule {

    @Provides
    @Singleton
    ApiService apiService(@Named("apiUrl") String apiUrl, OkHttpClient provideOkHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(apiUrl)
                //.client(provideOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(ApiService.class);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, StethoInterceptor stethoInterceptor, @Named("isDebugMode") boolean debugMode) {
        OkHttpClient okHttpClient;
        if (debugMode) {
            okHttpClient = new okhttp3.OkHttpClient()
                    .newBuilder()
                    .addNetworkInterceptor(stethoInterceptor)//this is to intercept network connection
                    .addInterceptor(httpLoggingInterceptor)//this is for retrofit to log all the request
                    .build();
        } else {
            okHttpClient = new OkHttpClient();
        }
        return okHttpClient;
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor provideLogginInterceptorRetrofit(@Named("isDebugMode") boolean debugMode) {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(debugMode ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return httpLoggingInterceptor;
    }

    @Singleton
    @Provides
    StethoInterceptor stethoInterceptorProvider() {
        return new StethoInterceptor();
    }

    @Provides
    @Singleton
    @Named("apiUrl")
    String provideApiUrl() {
        return BuildConfig.API_URL;
    }
}
