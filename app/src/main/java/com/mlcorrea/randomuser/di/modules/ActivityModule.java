package com.mlcorrea.randomuser.di.modules;

import android.app.Activity;

import com.mlcorrea.randomuser.di.scope.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuel on 10/06/17. *
 * <p>
 * A module to wrap the Activity state and expose it to the graph.
 */
@SuppressWarnings("unused")
@Module
public class ActivityModule {

    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
