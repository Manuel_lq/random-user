package com.mlcorrea.randomuser.di.modules.activity;

import com.mlcorrea.randomuser.di.scope.PerActivity;
import com.mlcorrea.randomuser.presenter.implentation.activity.MainActivityPresenter;
import com.mlcorrea.randomuser.presenter.implentation.fragment.UserListFragmentPresenter;
import com.mlcorrea.randomuser.ui.contract.activity.MainActivityContract;
import com.mlcorrea.randomuser.ui.contract.fragment.UserListFragmentContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuel on 10/06/17.
 */

@Module
public class MainActivityModule {

    @PerActivity
    @Provides
    UserListFragmentContract.Presenter provideUserListFragmentContractPresenter(UserListFragmentPresenter userListFragmentPresenter) {
        return userListFragmentPresenter;
    }

    @PerActivity
    @Provides
    MainActivityContract.Presenter mainActivityContractPresenter(MainActivityPresenter mainActivityPresenter) {
        return mainActivityPresenter;
    }

}
