package com.mlcorrea.randomuser.di;

/**
 * Created by manuel on 10/06/17.
 * <p>
 * Interface representing a contract for clients that contains a component for dependency injection.
 */

public interface HasComponent<C> {
    C getComponent();
}
