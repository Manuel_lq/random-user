package com.mlcorrea.randomuser.di.components.activity;

import com.mlcorrea.randomuser.di.components.ActivityComponent;
import com.mlcorrea.randomuser.di.components.AndroidApplicationComponent;
import com.mlcorrea.randomuser.di.modules.ActivityModule;
import com.mlcorrea.randomuser.di.modules.activity.UserDetailsActivityModule;
import com.mlcorrea.randomuser.di.scope.PerActivity;
import com.mlcorrea.randomuser.ui.activity.UserDetailsActivity;
import com.mlcorrea.randomuser.ui.fragment.UserDetailsFragment;

import dagger.Component;

/**
 * Created by manuel on 11/06/17.
 */

@PerActivity
@Component(dependencies = AndroidApplicationComponent.class, modules = {ActivityModule.class, UserDetailsActivityModule.class})
public interface UserDetailsActivityComponent extends ActivityComponent {

    void inject(UserDetailsActivity userDetailsActivity);

    void inject(UserDetailsFragment userDetailsFragment);
}
