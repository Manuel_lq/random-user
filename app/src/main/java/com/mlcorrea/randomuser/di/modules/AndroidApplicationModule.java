package com.mlcorrea.randomuser.di.modules;

import android.content.Context;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.di.DependencyInjector;
import com.mlcorrea.randomuser.BuildConfig;
import com.mlcorrea.randomuser.RandomUserApplication;
import com.mlcorrea.randomuser.business.interactor.UserCases;
import com.mlcorrea.randomuser.data.job.base.BaseJob;
import com.mlcorrea.randomuser.data.network.request.UserCasesImpl;
import com.mlcorrea.randomuser.utils.UtilsDevice;
import com.mlcorrea.randomuser.utils.logger.JobQueueLogger;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

/**
 * Created by manuel on 10/06/17.
 */
@SuppressWarnings("unused")
@Module
public class AndroidApplicationModule {

    private final RandomUserApplication randomUserApplication;


    public AndroidApplicationModule(RandomUserApplication randomUserApplication) {
        this.randomUserApplication = randomUserApplication;
    }

    @Singleton
    @Provides
    Context provideApplicationContext() {
        return this.randomUserApplication;
    }

    @Provides
    @Singleton
    EventBus eventBus() {
        return new EventBus();
    }

    @Singleton
    @Provides
    public Realm realm() {
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    JobManager jobManager() {
        Configuration.Builder builder = new Configuration.Builder(randomUserApplication)
                .consumerKeepAlive(45)//wait 45 seconds
                .maxConsumerCount(UtilsDevice.getNumberOfCores() + 1)
                .minConsumerCount(1)//always keep at least one consumer alive
                .loadFactor(3)
                .customLogger(JobQueueLogger.getJobLogger())
                .injector(new DependencyInjector() {
                    @Override
                    public void inject(Job job) {
                        if (job instanceof BaseJob) {
                            ((BaseJob) job).inject(randomUserApplication);
                        }
                    }
                });
        return new JobManager(builder.build());
    }

    @Provides
    @Singleton
    @Named("isDebugMode")
    boolean provideDegubMode() {
        return BuildConfig.DEBUG;
    }

    @Provides
    @Singleton
    UserCases provideUserCases(UserCasesImpl userCases){
        return userCases;
    }
}
