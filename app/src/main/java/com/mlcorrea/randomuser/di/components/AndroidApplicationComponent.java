package com.mlcorrea.randomuser.di.components;

import android.content.Context;

import com.birbit.android.jobqueue.JobManager;
import com.mlcorrea.randomuser.BaseApplication;
import com.mlcorrea.randomuser.RandomUserApplication;
import com.mlcorrea.randomuser.business.contract.ErrorMessageStrings;
import com.mlcorrea.randomuser.business.interactor.UserCases;
import com.mlcorrea.randomuser.business.repository.UserRepository;
import com.mlcorrea.randomuser.data.database.UserModel;
import com.mlcorrea.randomuser.data.database.wrapper.UserBDWrapper;
import com.mlcorrea.randomuser.data.job.request.GetListUserJob;
import com.mlcorrea.randomuser.data.network.api.ApiService;
import com.mlcorrea.randomuser.data.network.wrapper.UserDtoWrapper;
import com.mlcorrea.randomuser.di.modules.AndroidApplicationModule;
import com.mlcorrea.randomuser.di.modules.ApiModule;
import com.mlcorrea.randomuser.di.modules.DatabaseModule;
import com.mlcorrea.randomuser.di.modules.DtoWrapperModule;
import com.mlcorrea.randomuser.di.modules.ErrorsModule;
import com.mlcorrea.randomuser.errors.ErrorMapperStrings;
import com.mlcorrea.randomuser.errors.ErrorMessageCreator;
import com.mlcorrea.randomuser.ui.base.ActivityBase;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * Created by manuel on 10/06/17.
 */

@SuppressWarnings("unused")
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = {AndroidApplicationModule.class, ApiModule.class, ErrorsModule.class, DtoWrapperModule.class, DatabaseModule.class})
public interface AndroidApplicationComponent {

    void inject(RandomUserApplication randomUserApplication);

    void inject(BaseApplication baseApplication);

    void inject(ActivityBase activityBase);

    void inject(GetListUserJob getListUserJob);


    //Exposed to sub-graphs.
    Context context();

    EventBus eventBus();

    JobManager jobManager();

    @Named("isDebugMode")
    boolean debugMode();

    @Named("apiUrl")
    String apiUrlDomain();

    ApiService apiService();

    OkHttpClient okHttpClient();

    ErrorMessageStrings errorMessageStrings();

    ErrorMessageCreator errorMessageCreator();

    ErrorMapperStrings errorMapperStrings();

    UserDtoWrapper userDtoWrapper();

    UserBDWrapper userBdWrapper();

    UserModel userModel();

    UserRepository userRepository();

    UserCases userCases();
}
