package com.mlcorrea.randomuser.di.modules.activity;

import com.mlcorrea.randomuser.di.scope.PerActivity;
import com.mlcorrea.randomuser.presenter.implentation.activity.UserDetailsActivityPresenter;
import com.mlcorrea.randomuser.presenter.implentation.fragment.UserDetailsFragmentPresenter;
import com.mlcorrea.randomuser.ui.contract.activity.UserDetailsActivityContract;
import com.mlcorrea.randomuser.ui.contract.fragment.UserDetailsFragmentContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuel on 11/06/17.
 */

@Module
public class UserDetailsActivityModule {

    @PerActivity
    @Provides
    UserDetailsActivityContract.Presenter provideUserDetailsActivityContract(UserDetailsActivityPresenter userDetailsActivityPresenter) {
        return userDetailsActivityPresenter;
    }

    @PerActivity
    @Provides
    UserDetailsFragmentContract.Presenter provideUserDetailsFragmentContract(UserDetailsFragmentPresenter userDetailsFragmentPresenter) {
        return userDetailsFragmentPresenter;
    }

}
