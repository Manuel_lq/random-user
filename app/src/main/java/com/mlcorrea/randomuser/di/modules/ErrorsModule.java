package com.mlcorrea.randomuser.di.modules;

import com.mlcorrea.randomuser.business.contract.ErrorMessageStrings;
import com.mlcorrea.randomuser.errors.ErrorMapperStrings;
import com.mlcorrea.randomuser.errors.ErrorMessageCreator;
import com.mlcorrea.randomuser.errors.implementation.ErrorMapperStringsImpl;
import com.mlcorrea.randomuser.errors.implementation.ErrorMessageCreatorImpl;
import com.mlcorrea.randomuser.errors.implementation.ErrorMessageStringsImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuel on 10/06/17.
 */

@SuppressWarnings("unused")
@Module
public class ErrorsModule {

    @Provides
    @Singleton
    ErrorMessageStrings provideErrorMessageStrings(ErrorMessageStringsImpl errorMessageStringsImpl) {
        return errorMessageStringsImpl;
    }

    @Provides
    @Singleton
    ErrorMessageCreator provideErrorMessageCreator(ErrorMessageCreatorImpl errorMessageCreatorimpl) {
        return errorMessageCreatorimpl;
    }

    @Provides
    @Singleton
    ErrorMapperStrings provideErrorMapperStrings(ErrorMapperStringsImpl errorMapperStringsImpl) {
        return errorMapperStringsImpl;
    }
}