package com.mlcorrea.randomuser.di.components;

import android.app.Activity;

import com.mlcorrea.randomuser.di.modules.ActivityModule;
import com.mlcorrea.randomuser.di.scope.PerActivity;

import dagger.Component;

/**
 * Created by manuel on 10/06/17.
 * <p>
 * A base component upon which fragment's components may depend.
 * Activity-level components should extend this component.
 * <p>
 * Subtypes of ActivityComponent should be decorated with annotation:
 */
@SuppressWarnings("unused")
@PerActivity
@Component(dependencies = AndroidApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    //Exposed to sub-graphs.
    Activity activity();

}