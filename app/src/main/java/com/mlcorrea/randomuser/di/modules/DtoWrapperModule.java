package com.mlcorrea.randomuser.di.modules;

import com.mlcorrea.randomuser.data.network.wrapper.UserDtoWrapper;
import com.mlcorrea.randomuser.data.network.wrapper.implentation.UserDtoWrapperImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuel on 10/06/17.
 */

@SuppressWarnings("unused")
@Module
public class DtoWrapperModule {

    @Provides
    UserDtoWrapper provideUserDtoWrapper(UserDtoWrapperImpl userDtoWrapperImpl){
        return userDtoWrapperImpl;
    }
}
