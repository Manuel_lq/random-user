package com.mlcorrea.randomuser;

import android.app.Application;

/**
 * Created by manuel on 10/06/17.
 */

public class RandomUserApplication extends BaseApplication {

    @Override
    protected RandomUserApplication getRandomUserApplication() {
        return this;
    }
}
