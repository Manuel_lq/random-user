package com.mlcorrea.randomuser;

import android.os.StrictMode;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import timber.log.Timber;

/**
 * Created by manuel on 10/06/17.
 */

public class RandomUserApplication extends BaseApplication {

    @Override
    protected RandomUserApplication getRandomUserApplication() {
        return this;
    }

    @Override
    public void onCreate() {
        Timber.plant(new Timber.DebugTree() {
            @Override
            protected String createStackElementTag(StackTraceElement element) {
                return super.createStackElementTag(element) + ":" + element.getLineNumber();
            }
        });
        super.onCreate();

        strictMode();
        initializeStetho();
    }

    /**
     * this is only for testing purposes and it only should be used in debug mode, it check ARN error and others things
     */
    private void strictMode() {
        StrictMode.setThreadPolicy(new
                StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new
                StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
    }

    private void initializeStetho() {
        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                .build());
    }
}

